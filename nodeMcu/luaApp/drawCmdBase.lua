function drawCmdBase(id)
  disp:clearBuffer()    
  disp:drawRBox(0,0,128,16,4)
  disp:drawRFrame(0,16,128,48,4)
  disp:setDrawColor(0)
  local str = "Command id 0x"
  str = str .. string.upper(string.format("%02x", id))
  local x = disp:getStrWidth(str)
  disp:drawStr((128 - x)/2, 4, str)
  disp:setDrawColor(1)    
  disp:sendBuffer()
end
return drawCmdBase;
