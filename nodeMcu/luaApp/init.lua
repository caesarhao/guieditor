disp = nil;

local function init_i2c_display()
    local sda = 1 -- GPIO14
    local scl = 2 -- GPIO12
    local sla = 0x3c
    i2c.setup(0, sda, scl, i2c.SLOW)
--    disp = u8g2.ssd1306_i2c_128x64_noname(0, sla)
    disp = u8g2.sh1106_i2c_128x64_noname(0, sla)
end

  init_i2c_display()
  disp:clearBuffer()
  disp:setFont(u8g2.font_6x10_tf)
  disp:setFontRefHeightExtendedText()
  disp:setDrawColor(1)
  disp:setFontPosTop()
  disp:setFontDirection(0)
  dofile("showLogo.lua")
  
  print("Press any key to stop startup process...")
  local startupTimer = tmr.create()
  startupTimer:register(5000, tmr.ALARM_SINGLE, 
    function (t) 
     uart.on("data")
     print("Starting...")
     dofile("main.lua")     
     t:unregister() 
    end)
  uart.on("data", "\r", function(data)
    startupTimer:unregister()
    uart.on("data")
    print("Startup stoped. Console enabled")
    print("> ")
    end, 
  0)  
  startupTimer:start()

