function ret = setFilterA(obj, a)
  pkgLen = 1 + (8*length(a)) + 5;
  v = linspace(uint8(0), uint8(0), pkgLen);
  v(1) =  176;                % Header
  v(2) = (length(a) * 8) + 1; % Largo del paquete
  v(3) = 6;                   % CMD
  v(4) = length(a);           % Cantidad de terminos
  
  for k=1:length(a)
    dataBits = bitunpack(double(a(k)));
    bitInit = 1;
    bitEnd = 8;
    for i=1:8 
      bytePart = uint8(bitpack(dataBits(bitInit:bitEnd), "uint8"));
      v(4 + ((k - 1) * 8) + i) = bytePart;
      bitInit = bitInit + 8;
      bitEnd = bitEnd + 8;
    endfor
  endfor
  v(length(v)) = 11;
  
  crcPos =  length(v)-1;
  v(crcPos) = 0;
  
  for i=1:crcPos - 1
    v(crcPos) = v(crcPos) + v(i);
  endfor
  v(crcPos) = mod(v(crcPos), 256);      
  
  port = obj.serialDev;
  srl_flush(port);
  for i=1:length(v)
    srl_write(port, uint8(v(i)));
  endfor
  ret = "";
  letra = char(srl_read(port, 1));  
  while ((letra != char(10)))  
    ret = [ret letra];
    letra = char(srl_read(port, 1));
  endwhile  
endfunction
