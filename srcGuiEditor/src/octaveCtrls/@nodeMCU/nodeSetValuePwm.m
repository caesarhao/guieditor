function nodeSetValuePwm(obj, nroPin, frec, duty)
  v = [176, 5, nroPin, 0, 0, 0, 0, 0, 11];
  if (strcmp(class(obj), "nodeMCU") != 1)  
    error("Esta funcion ha sido definida para utilizar objetos de la clase nodeMCU");
  endif
  if (frec > 1000) frec = 1000; endif
  if (duty > 1023) duty = 1023; endif
  if (frec < 1)    frec = 1; endif 
  if (duty < 0)    duty = 0; endif 
  v(5) = mod(frec, 256);
  frec = idivide (frec, 256);
  v(4) = mod(frec, 256);
        
  v(7) = mod(duty, 256);
  duty = idivide (duty, 256);
  v(6) = mod(duty, 256);
  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  for i=1:9
    tcp_write(obj.socket, uint8(v(i)));
  endfor
 endfunction
