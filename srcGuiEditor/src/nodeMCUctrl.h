/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef NODEMCUCTRL_H
#define NODEMCUCTRL_H

#include <QWidget>
#include "abstractuicctrl.h"
#include <QLabel>
#include "commonproperties.h"
#include "controlgenerator.h"

class showIpDialog: public abstractPropEditor
{
  Q_OBJECT
private:
    bool showIpDlg;
public:
    showIpDialog(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual bool canGenerateCode();
    virtual QWidget * getEditWidget();
public slots:
  virtual void callBackChanged(int state);
};


class expNodeClass: public abstractPropEditor
{
  Q_OBJECT
private:
    bool expClass;
public:
    expNodeClass(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual bool canGenerateCode();
    virtual QWidget * getEditWidget();
public slots:
  virtual void callBackChanged(int state);
};

class edIpEditor: public abstractPropEditor
{
    Q_OBJECT
  private:
    QString ip;
  public:
    edIpEditor(abstractUICCtrl *ctrl);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
};

class cbPinNodeTypeEditor: public abstractPropEditor
{
    Q_OBJECT
  private:
    int pinNumber;
    bool pinIsReadOnly;
  public:
    cbPinNodeTypeEditor(abstractUICCtrl *ctrl, int pinNumber, bool isReadOnly = false);
    virtual QString getValue();
    virtual bool isValidValue(QString newVal);
    virtual void setValue(QString newVal);
    virtual QString generateCode();
    virtual QWidget * getEditWidget();
  public slots:
    virtual void cbPinDirChanged(QString text);
};


class nodeMCUCtrl : public abstractUICCtrl
{
private:
  static unsigned int uicNameCounter;  

public:
    static unsigned int getNameCounter();
    nodeMCUCtrl(QWidget * parent, abstractUICCtrl *octaveParent, childWndDlg * parentWnd);
    QStringList createCallBack(QString path);
    void setParents(QWidget * parent, abstractUICCtrl * octaveParent, childWndDlg * parentWnd);
    QStringList generateMFile(QString path);
    abstractUICCtrl * clone(QWidget *parent,
                            abstractUICCtrl *octaveParent,
                            childWndDlg * parentWnd);
    void setHidden(bool);
    virtual ~nodeMCUCtrl();
    virtual QString className() { return "nodeMCUCtrl";}
    virtual bool isAutoSize() { return true;}
};


class nodeMCUCtrlGen: public controlGenerator
{
public:
  nodeMCUCtrlGen();
  virtual abstractUICCtrl * getControl(QXmlStreamReader &xml, abstractUICCtrl *parent, childWndDlg * dlg);
};

#endif // NODEMCU_H
