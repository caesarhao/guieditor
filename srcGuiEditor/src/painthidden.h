#ifndef PAINTHIDDEN_H
#define PAINTHIDDEN_H

#include <QWidget>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QTableWidget>

/*
 This class generates the representation of
 controls that have been marked as hidden.
 Different methods are used because it was
 not possible to use a single form of representation
 to achieve the same graphic effect.
*/
class paintHidden
{
public:
    paintHidden(QWidget *w);
    static QString getTableHiddenStyle(void);
    static QString getListHiddenStyle(void);
};

#endif // PAINTHIDDEN_H
