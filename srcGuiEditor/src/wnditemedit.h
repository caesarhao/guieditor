/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef WNDITEMEDIT_H
#define WNDITEMEDIT_H

#include <QDialog>

namespace Ui {
class wndItemEdit;
}

class wndItemEdit : public QDialog
{
    Q_OBJECT

public:
    explicit wndItemEdit(QWidget *parent = 0);
    ~wndItemEdit();
    Ui::wndItemEdit *ui;


private slots:
    void on_btnAdd_clicked();
    void on_btnDel_clicked();
    void on_btnUp_clicked();
    void on_btnDown_clicked();
    void on_btnAceptar_clicked();
};

#endif // WNDITEMEDIT_H
