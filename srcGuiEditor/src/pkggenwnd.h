/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef PKGGENWND_H
#define PKGGENWND_H

#include <QDialog>
#include <QProcess>

namespace Ui {
class pkgGenWnd;
}

class pkgGenWnd : public QDialog
{
    Q_OBJECT

public:
    explicit pkgGenWnd(QWidget *parent = 0);
    ~pkgGenWnd();
    void startGen(QString path, QString pkgName);
private:
    Ui::pkgGenWnd *ui;
    QProcess *gzip;
    QProcess *tar;
    QString path;
    QString pkgName;
protected slots:
    void tarStarted();
    void tarFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void tarReadyReadOutput();
    void tarReadyReadError();
    void gzipStarted();
    void gzipFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void gzipReadyReadOutput();
    void gzipReadyReadError();

private slots:
    void on_btnOpen_clicked();
    void on_btnClose_clicked();
};

#endif // PKGGENWND_H
