/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "scleaveprg.h"

scLeavePrg::scLeavePrg(prgAction *act):serialCmd(act, true)
{
    this->buf.clear();
}

QString scLeavePrg::cmdInfo()
{
    return "Leave programming mode cmd";
}

QByteArray scLeavePrg::dataToSend(void)
{
    QByteArray toSend;
    toSend.push_back(Cmnd_STK_LEAVE_PROGMODE);
    toSend.push_back(Sync_CRC_EOP);
    return toSend;
}

rcvType scLeavePrg::processResponse(QByteArray data)
{
    rcvType ret = rcvWait;
    buf.push_back(data);
    if ((buf.length() == 2) && (buf[0] == Resp_STK_INSYNC) && (buf[1] == Resp_STK_OK))
        ret = rcvOk;
    else if((buf.length() == 1) && (buf[0] == Resp_STK_NOSYNC))
        ret = rcvFail;
    return ret;
}

scLeavePrg::~scLeavePrg(void)
{

}
