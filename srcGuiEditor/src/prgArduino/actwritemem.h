/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#ifndef ACTWRITEMEM_H
#define ACTWRITEMEM_H
#include "prgaction.h"
#include "hexfile.h"
#include "scwritemem.h"
#include "sccheckmem.h"
#include "scloadpage.h"
class actWriteMem : public prgAction
{
protected:
    scCheckMem * cmdCheck;
    scWriteMem * cmdWrite;
    scLoadPage * cmdLoadCh;
    scLoadPage * cmdLoadWr;
    memPage *p;    

public:
    actWriteMem(memPage *p) : prgAction()
    {
        cmdLoadCh  = new scLoadPage(this, p);
        cmdCheck = new scCheckMem(this, p, false);
        cmdLoadWr  = new scLoadPage(this, p);
        cmdWrite = new scWriteMem(this, p);


        this->p = p;
    }

    bool mustWrite(void)
    {
        return !cmdCheck->checkOk();
    }


    QString info()
    {
        QString ret;
        ret = "Check/Write addres: " + QString("0x%1").arg(p->getAddr(), 4, 16, QLatin1Char('0'));
        if(cmdCheck->checkOk())
            ret = ret + "[skip]";
        else
            ret = ret + "[write]";
        return ret;
    }
    void getSerialCmd(QVector<serialCmd *> &toSend)
    {
        toSend.push_back(cmdLoadCh);
        toSend.push_back(cmdCheck);
        toSend.push_back(cmdLoadWr);
        toSend.push_back(cmdWrite);
    }
    ~actWriteMem(){
        delete cmdLoadCh;
        delete cmdLoadWr;
        delete cmdCheck;
        delete cmdWrite;
    }
};

#endif // ACTWRITEMEM_H
