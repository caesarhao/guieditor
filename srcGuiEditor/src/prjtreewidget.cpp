/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "prjtreewidget.h"
#include <QMenu>

prjTreeWidget::prjTreeWidget(QWidget *parent) :
    QTreeWidget(parent)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            SLOT(showContextMenu(const QPoint&)));
}

void prjTreeWidget::showContextMenu(const QPoint &pos)
{
  QMenu contextMenu;

  abstractPrjTreeItem* item = dynamic_cast<abstractPrjTreeItem*> (itemAt(pos));
  if(item)
  {
    item->populateMenu(contextMenu);
    contextMenu.exec(mapToGlobal(pos));
  }
}
