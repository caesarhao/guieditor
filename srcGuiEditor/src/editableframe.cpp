/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "editableframe.h"
#include <QApplication>
#include "framedlg.h"
#include <QFontMetrics>
#include "mainwnd.h"
#include "ui_mainwnd.h"
#include "painthidden.h"

editableFrame::editableFrame(QWidget *parent, abstractUICCtrl *uiCtrl) :
    QFrame(parent)
{    
    underMovement = false;
    this->parentUICtrl = uiCtrl;
    this->setFocusPolicy(Qt::StrongFocus);
    this->isHidden = false;
}

void editableFrame::setParents(QWidget *parent, abstractUICCtrl *uiCtrl)
{
    setParent(parent);
    this->parentUICtrl = uiCtrl;
}

void editableFrame::setHidden(bool isHidden)
{
    this->isHidden = isHidden;
}

editableFrame::~editableFrame()
{

}

int editableFrame::cordToGrid(int v)
{
    int ret;
    ret = v / 5;
    ret = ret * 5;
    return ret;
}

void editableFrame::mouseReleaseEvent(QMouseEvent *)
{
    underMovement = false;
    this->parentUICtrl->updateAdjPoints();
}

void editableFrame::mousePressEvent(QMouseEvent * event)
{
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();

    underMovement = true;
    moveXInit = this->cordToGrid(event->x());
    moveYInit = this->cordToGrid(event->y());

    if(!wnd->isSelected(this->parentUICtrl))
      wnd->addToSelection(this->parentUICtrl);
}

void editableFrame::mouseMoveEvent(QMouseEvent * event)
{
    int dx;
    int dy;
    childWndDlg *wnd = this->parentUICtrl->getParentWnd();

    if(underMovement)
    {
        dx = this->cordToGrid(event->x()) - moveXInit;
        dy = this->cordToGrid(event->y()) - moveYInit;
        wnd->moveSelection(dx, dy);
    }
}


void editableFrame::moveEvent ( QMoveEvent * event )
{
    QFrame::moveEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableFrame::resizeEvent ( QResizeEvent * event )
{
    QFrame::resizeEvent(event);
    this->parentUICtrl->updateAdjPoints();
    this->parentWidget()->update();
    mainWnd::getPropPan()->updatePropertiesValues();
}

void editableFrame::keyPressEvent(QKeyEvent *e)
{
    if (e->type() == QEvent::KeyPress)
    {
        QKeyEvent* newEvent = new QKeyEvent(QEvent::KeyPress,e->key(), e->modifiers ());
        qApp->postEvent (this->parent(), newEvent, 0);
    }
}

void editableFrame::paintEvent(QPaintEvent *e){
    QFrame::paintEvent(e);
    if(isHidden)
        paintHidden ph(this);
}

