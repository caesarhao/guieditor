/****************************************************************************
Copyright 2015-2020, Enrique Sergio Burgos

This file is part of GUI Editor for Octave.

GUI Editor for Octave is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GUI Editor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GUI Editor.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/
#include "mainwnd.h"
#include <QApplication>
#include "configsettings.h"
#include <QTranslator>
#include <QPixmap>
#include <QSplashScreen>

int main(int argc, char *argv[])
{
    QTranslator translator;
    QApplication a(argc, argv);
    configSettings *appCfg = new configSettings();
    int ret = 0;
    if(appCfg->getLanguage() == "Spanish")
       translator.load("srcGuiEditor_sp", a.applicationDirPath());
    else if(appCfg->getLanguage() == "English")
       translator.load("srcGuiEditor_en", a.applicationDirPath());
    a.installTranslator(&translator);
    mainWnd w(appCfg);
    QPixmap logo(":/img/img/startUpImg.png");
    QSplashScreen splash(logo);
    splash.show();
    a.processEvents();
    w.show();
    ret =  a.exec();
    appCfg->saveCfg();
    delete appCfg;
    
    return ret;
}
