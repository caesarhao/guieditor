#include "wdgiconchooser.h"
#include "ui_wdgiconchooser.h"
#include "iconchooserwnd.h"
#include "iconchooserwnd.h"
#include <QDir>
#include "buttonctrl.h"

wdgIconChooser::wdgIconChooser(abstractPropEditor *prop) :
    QWidget(0),updPropWdg(prop),
    ui(new Ui::wdgIconChooser)
{
    ui->setupUi(this);
}

void wdgIconChooser::setIconFileName(QString fn){
    this->iconFileName = fn;
    this->ui->labIconName->setText(fn);
    this->ui->labIcon->setPixmap(QPixmap(this->imgPath + QDir::separator() + fn));
    ((buttonCtrl *)this->getPropEditor()->getCtrl())->setIconFile(fn);
}

QString wdgIconChooser::getIconFileName(void){
    return this->iconFileName;
}


void wdgIconChooser::updPropValue()
{
    //ui->leColor->setText(this->getPropEditor()->getValue());
}

wdgIconChooser::~wdgIconChooser()
{
    delete ui;
}

void wdgIconChooser::on_btnChangeIcon_clicked()
{
    abstractUICCtrl *ctrl = getPropEditor()->getCtrl();
    childWndDlg *wdg = ctrl->getParentWnd();
    guiProject * prj = wdg->getGUIPrj();
    /*
     This test is for back compatibility, a dialog can or not be part of project.
    */
    if(prj != NULL){
      this->imgPath = prj->imgPath();
      QList<projectItem *>::iterator it;
      for (it = prj->items.begin(); it != prj->items.end(); it++){
        if((*it)->groupName() == "Image"){
            this->imgList.push_back((*it)->getName());
        }
      }
    }
    iconChooserWnd ich(this, this->imgList, this->imgPath, this->iconFileName);
    if(ich.exec() == QDialog::Accepted){
        if(ich.getSelectedIcon() != tr("[None]"))
          setIconFileName(ich.getSelectedIcon());
        else
          setIconFileName("");
    }
}
